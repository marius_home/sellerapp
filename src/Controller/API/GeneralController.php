<?php
namespace App\Controller\API;

use App\Entity\General;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GeneralController extends AbstractController
{
    /**
     * @Route("/api/getGeneralByType", name="apiGetGeneralByType", methods={"POST"})
     */
    public function getGeneralByType($type): JsonResponse
    {
        $settings = $this->doctrine->getRepository(General::class)->findBy(["type" => $type]);
        return new JsonResponse(['general' => $settings], Response::HTTP_OK);
    }

    /**
     * @Route("/api/getCityByCountyCode", name="apiGetCityByCountyCode", methods={"POST"})
     */
    public function getCityByCountyCode($code): JsonResponse
    {
        $city = $this->doctrine->getRepository(General::class)->findBy(["type" => General::USER_CITY, "value1" => $code]);
        return new JsonResponse(['city' => $city], Response::HTTP_OK);
    }
}