<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mime\Email;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class SendEmail
{
    public function send(Environment $twig, TranslatorInterface $translator, $to, $subject, $params = [], $template): JsonResponse
    {

        $transport = Transport::fromDsn($_ENV['MAILER_DSN']);
        $mailer = new Mailer($transport);

        $htmlContents = $twig->render($template, $params);

        $email = (new Email())
            ->from($_ENV['SMTP_EMAIL'])
            ->to($to)
            ->subject($subject)
            ->html($htmlContents);

        $mailer->send($email);
        return new JsonResponse();
    }
}